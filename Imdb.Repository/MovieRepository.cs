﻿using System;
using System.Collections.Generic;
using System.Linq;
using Imdb.Domain;

namespace Imdb.Repository
{
    public class MovieRepository
    {
        private readonly List<Movie> _movies;

        public MovieRepository()
        {
            _movies = new List<Movie>();
        }

        public void Add(Movie movie)
        {
            _movies.Add(movie);
     
        }
        public void Remove(String name)
        {
            _movies.RemoveAll(b => b.Name == name);


        }

        public void Example()
        {

            var actor = from movie in _movies
                        from act in movie.Actors

                        where movie.Actors.Count < 2 && (DateTime.Now.Year)-(act.DateOfBirth.Year)>18

                        select act;
        }
        public List<Movie> Get()
        {
            return _movies.ToList();
        }
    }
}


