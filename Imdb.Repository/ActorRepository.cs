﻿using Imdb.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Imdb.Repository
{
    public class ActorRepository
    {
        private readonly List<Person> _actors;

        public ActorRepository()
        {
            _actors = new List<Person>();
        }

        public void Add(Person actor)
        {
            _actors.Add(actor);
        }

        public List<Person> Get()
        {
            return _actors.ToList();
        }
    }
}
