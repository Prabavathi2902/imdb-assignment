﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Imdb.Domain;

namespace Imdb.Repository
{
    public class ProducerRepository
    {
        private readonly List<Person> _producer;

        public ProducerRepository()
        {
            _producer = new List<Person>();
        }

        public void Add(Person producer)
        {
            _producer.Add(producer);
        }

        public List<Person> Get()
        {
            return _producer.ToList();
        }
    }
}
