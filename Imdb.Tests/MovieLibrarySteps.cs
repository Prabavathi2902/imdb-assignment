﻿using System;
using TechTalk.SpecFlow;
using System.Collections.Generic;
using Imdb;
using Imdb.Domain;
using TechTalk.SpecFlow.Assist;
using Imdb.Repository;
using System.Collections;
using Xunit;
using System.Linq;


namespace Imdb.Tests
{
    [Binding]
    public class MovieLibrarySteps
    {
        private readonly ImdbService _movie;
  
        private string _name, _plot;
        private int _year;
        private Exception _exception;
        private List<Movie> _movielist;
        private List<Person> _actorsName ;
        private Person _producerName;
        
    

        public MovieLibrarySteps()
        {
            
            _movie = new ImdbService();
          
        }

        [Given(@"I have a movie with name ""(.*)""")]
        public void GivenIHaveAMovieWithName(string name)
        {
            _name = name;
        }
        
        [Given(@"the Year of release is ""(.*)""")]
        public void GivenTheYearOfReleaseIs(int year)
        {
            _year = year;
        }
        
        [Given(@"plot is ""(.*)""")]
        public void GivenPlotIs(string plot)
        {
            _plot = plot;
        }
  
        [When(@"I add the movie into the Movielist")]
        public void WhenIAddTheMovieIntoTheMovielist()
        {

            try
            {
                
                _movie.AddMovie(_name, _year,_plot,_actorsName,_producerName);
            }
            catch (Exception ex)
            {
                _exception = ex;
            }
        }

        [Then(@"I should have an error ""(.*)""")]
        public void ThenIShouldHaveAnError(string message)
        {
            Assert.Equal(message, _exception.Message);
        }

        [When(@"I list my Movies")]
        public void WhenIListMyMovies()
        {

            _movielist=_movie.GetMovies();
            
        }

        
        [Then(@"my Movielist would look like this")]
        public void ThenMyMovielistWouldLookLikeThis(Table table)
        {

            
            _movielist = _movie.GetMovies();
            table.CompareToSet(_movielist);
        }


        [Then(@"I should have the following Movies")]
        public void ThenIShouldHaveTheFollowingMovies(Table table)
        {
            table.CompareToSet(_movielist);
        }

        [Then(@"I should have the follwing Actors")]
        public void ThenIShoulsHaveTheFollwingActors(Table table)
        {
            var actorLists = new List<Person>();
            _movielist.ForEach(b=>actorLists.AddRange(b.Actors));
            table.CompareToSet(actorLists);

        }

        [Then(@"I should have the following Producers")]
        public void ThenIShoulsHaveTheFollowingProducers(Table table)
        {
            table.CompareToSet(_movielist.Select(a => a.Producer).ToList());

        }


        [Given(@"Actors are ""(.*)""")]
        public void GivenIActorsAre(string choose)
        {
           _actorsName= _movie.ChooseActor(choose);
           


        }

        [Given(@"producer is ""(.*)""")]
        public void GivenIProducerIs(string producer)
        {
            
            _producerName=_movie.ChooseProducer(producer);
          
        }

 
        [BeforeScenario("addMovie","listMovie")]
        public void AddActorsAndProducers()
        {
           
         
            _movie.AddActor("Matt Damon", "1960/12/12");
    
            _movie.AddActor("Christian Bale", "1970/10/11");

            _movie.AddProducer("James Mangold", "1965/06/19");
      
           _movie.AddProducer("Sk", "1980/03/12");
          
        }

        [BeforeScenario("listMovie")]
        public void AddSampleMovieToList()
        {
            List<Person> actorsName = _movie.ChooseActor("1 2");
            Person producerName = _movie.ChooseProducer("1");
            _movie.AddMovie("Ford v Ferrari", 2019, "American car designer Carroll Shelby and driver Ken Miles battle corporate interference, the laws of physics and their own personal demons to build a revolutionary race car for Ford and challenge Ferrari at the 24 Hours of Le Mans in 1966.", actorsName, producerName);
             List<Person> actorsName1 = _movie.ChooseActor("1 2");
            Person producerName1 = _movie.ChooseProducer("2");
            _movie.AddMovie("Master", 2020, "XYZ", actorsName1, producerName1);

        }
    }
   
}






