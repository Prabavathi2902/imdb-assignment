﻿Feature: Movie Library
	To manage my movielist, I want a system that does this for me.

@addMovie	
Scenario: Adding a Movie to Movielist
	Given I have a movie with name "Ford v Ferrari"
	And the Year of release is "2019"
	And plot is "American car designer Carroll Shelby and driver Ken Miles battle corporate interference, the laws of physics and their own personal demons to build a revolutionary race car for Ford and challenge Ferrari at the 24 Hours of Le Mans in 1966."
	And Actors are "1 2"
	And producer is "1"
	When I add the movie into the Movielist
	Then my Movielist would look like this
	| Name           | Year | Plot                                                                                                                                                                                                                                            |
	| Ford v Ferrari | 2019 | American car designer Carroll Shelby and driver Ken Miles battle corporate interference, the laws of physics and their own personal demons to build a revolutionary race car for Ford and challenge Ferrari at the 24 Hours of Le Mans in 1966. |	
	And I should have the following Producers
	| Name          | DateOfBirth            |
	| James Mangold | 6/19/1965 12:00:00 AM |
	And I should have the follwing Actors
	| Name           | DateOfBirth            |
	| Matt Damon     | 12/12/1960 12:00:00 AM |
	| Christian Bale | 10/11/1970 12:00:00 AM |
	


@addMovie
Scenario: Dont add empty Movie name
	Given I have a movie with name ""
	And the Year of release is "2020"
	And plot is "XYZ"
	And Actors are "1 2"
	And producer is "1"
	When I add the movie into the Movielist
	Then I should have an error "Invalid arguments"
	



@listMovie
Scenario: List all Movie from Movielist
	When I list my Movies
	Then I should have the following Movies
	| Name           | Year | Plot                                                                                                                                                                                                                                            |
	| Ford v Ferrari | 2019 | American car designer Carroll Shelby and driver Ken Miles battle corporate interference, the laws of physics and their own personal demons to build a revolutionary race car for Ford and challenge Ferrari at the 24 Hours of Le Mans in 1966. |
	| Master         | 2020 | XYZ                                                                                                                                                                                                                                             |
	And I should have the follwing Actors
	| Name           | DateOfBirth            |
	| Matt Damon     | 12/12/1960 12:00:00 AM |
	| Christian Bale | 10/11/1970 12:00:00 AM |
	| Matt Damon     | 12/12/1960 12:00:00 AM |
	| Christian Bale | 10/11/1970 12:00:00 AM |
	And I should have the following Producers
	| Name          | DateOfBirth           |
	| James Mangold | 6/19/1965 12:00:00 AM |
	| Sk            | 3/12/1980 12:00:00 AM |
	