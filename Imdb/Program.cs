﻿using Imdb.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Imdb
{
    class Program
    {
        static void Main(string[] args)
        {
            var imdbservice = new ImdbService();
            bool flag = true;

            while (flag)
            {
                try
                {
                    Console.WriteLine(" 1) List Movies \n 2) Add Movie \n 3) Add Actor \n 4) Add Producer \n 5) Delete Movie \n 6) Exit");
                    Console.WriteLine("What do you want to do");
                    var select = Convert.ToInt32(Console.ReadLine());
                    switch (select)
                    {
                        case 1:

                            var movielist = imdbservice.GetMovies();
                            if (movielist.Count == 0)
                            {
                                Console.WriteLine("No movies present in the movielist");
                            }
                            for (var i = 0; i < movielist.Count; i++)
                            {
                                Console.WriteLine("{0} ({1})", movielist[i].Name, movielist[i].Year);
                                Console.WriteLine("Plot-{0}", movielist[i].Plot);
                                var actorTotalList = movielist[i].Actors.Select(actor=>actor.Name).ToArray();
                                Console.WriteLine("Actors-{0}", string.Join(',', actorTotalList));
                                Console.WriteLine("Producer-{0}", movielist[i].Producer.Name);
                            }
                            break;
                        case 2:
                            Console.Write("Name:");
                            var moviename = Console.ReadLine();
                            Console.Write("Year Of Release:");
                            int yearOfRelease = Convert.ToInt32(Console.ReadLine());
                            Console.Write("Plot:");
                            var plot = Console.ReadLine();
                            Console.Write("Choose actor(s):");
                            var actorList = imdbservice.GetActors();
                            for (var i = 0; i < actorList.Count; i++)
                            {
                                Console.Write(" {0}.{1} ", i + 1, actorList[i].Name);
                            }
                            Console.WriteLine();
                            string choose = Console.ReadLine();
                            List<Person> actorsName = imdbservice.ChooseActor(choose);
                            Console.WriteLine();
                            Console.Write("Choose Producer");
                            var producerList = imdbservice.GetProducer();

                            for (var i = 0; i < producerList.Count; i++)
                            {
                                Console.Write(" {0}.{1} ", i + 1, producerList[i].Name);
                            }
                            Console.WriteLine();
                            //int producerSelect = 0;
                            string producerSelect = Console.ReadLine();
                            Person producerName = imdbservice.ChooseProducer(producerSelect);
                            imdbservice.AddMovie(moviename, yearOfRelease, plot, actorsName, producerName);
                            break;
                        case 3:

                            Console.Write("Name:");
                            string name = Console.ReadLine();
                            Console.Write("DOB:");
                            string dob = Console.ReadLine();
                            imdbservice.AddActor(name, dob);
                            break;
                        case 4:
                            Console.Write("Name:");
                            string name1 = Console.ReadLine();
                            Console.Write("DOB:");
                            string dob1 = Console.ReadLine();
                            imdbservice.AddProducer(name1, dob1);
                            break;
                        case 5:
                            Console.Write("Movie Name:");
                            string deleteMovieName = Console.ReadLine();
                            imdbservice.DeleteMovie(deleteMovieName);
                            break;
                        case 6:
                            flag = false;
                            break;
                        default:
                            Console.WriteLine("Enter valid number");
                            break;
                    }
                }
                catch (Exception ex)
                {

                    Console.WriteLine(ex.Message);
                }
            }

               

            
        }

        }
    }
    
